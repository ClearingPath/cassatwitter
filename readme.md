# CassaTwitter 
##### Aplikasi Twitter dengan menggunakan Cassandra sebagai database

Gunakan aplikasi NetBeans untuk building file project NetBeans CassaTwitter. Cara menjalankan program adalah menjalankan terminal pada lokasi file ```.jar``` lalu menjalankan aplikasi dengan perintah-perintah berikut;
```sh
$ java -jar "CassaTweet.jar"
```

## Penggunaan Program

Program akan meminta login dengan mengisi username dan password (jika belum ada masukkan username dan password baru).
```sh
$ Enter username : <username>
$ Password : <password>
```
Jika tidak ditemukan akan muncul pertanyaan untuk membuat username baru menggunakan inputan yang terakhir.
```sh
$ Do you want to create that username (y/N) ? [y|n]
```
Client dapat menambah teman dengan perintah :
```sh
$ /add <friends_name>
```
Client dapat lihat daftar teman dengan perintah :
```sh
$ /friends
```
Client dapat melakukan tweet dengan :
```sh
$ /tweet <isi_tweet>
```
Client dapat melihat userline dari user tertentu dengan perintah :
```sh
$ /userline <username>
```
Client dapat melihat timeline dengan perintah :
```sh
$ /timeline
```
Untuk logout dapat menggunakan perintah :
```sh
$ /logout
```
Untuk keluar dari aplikasi, pengguna dapat memasukkan perintah sebagai berikut;
```sh
$ /exit
```


## Testing Program
#### 1. Login dan create username
*Hasil testing*: Sukses.  
User dapat login dengan username dan password yang sudah ada di database atau user dapat membuat username dengan password yang telah diberikan.
*Cara testing*: Menjalankan program kemudian mengisi username dan password. Jika username dan password tidak ditemukan maka akan muncul pertanyaan membuat username.
#### 2. Add dan view friends  
*Hasil testing*: Sukses.  
Bisa menambahkan teman dan melihat daftar teman.  
*Cara testing*: Menambahkan teman dengan ```/add <nama teman>``` dan view dengan ```/friends```.
#### 3. Menambahkan tweet
*Hasil testing*: Sukses.   
Melakukan tweeting sehingga seluruh teman dapat melihat hasil tweet. 
*Cara testing*: Melakukan tweet dengan ```/tweet <pesan>```.
#### 4. Melihat userline dan timeline  
*Hasil testing*: Sukses.  
Melihat tweet seorang user dan melihat timeline dari user sendiri
*Cara testing*: Menjalankan perintah ```/userline <username>``` dan untuk timeline ```/timeline```.
#### 5. Logout
*Hasil testing*: Sukses.  
Melakukan logout dari user saat ini 
*Cara testing*: Menjalankan perintah ```/logout```.
#### 6. Exit   
*Hasil testing*: Sukses.  
Program menampilkan pesan sukses dan program client berhenti.   
*Cara testing*: Memasukkan perintah ```/EXIT```.
#### 7. Jika hanya memasukan satu perintah (tanpa lanjutan)
*Hasil testing*: Sukses.  
Program dapat meminta input lanjutan sehingga perintah dapat dieksekusi secara baik.   
*Cara testing*: Memasukkan perintah ```/add``` atau ```/userline``` atau ```/tweet```.

## Copyright
* Felicia Christie 13512039
* Jonathan Sudibya 13512093

