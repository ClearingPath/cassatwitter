import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * Created by jojo on 11/11/15.
 */
public class CassaMain {

    public static String serverIP = "167.205.35.19";
    public static String keyspace = "cassatweetclp";
    public static User CurrentUser;

    public static UserLine CurrentUserline;
    public static TimeLine CurrentTimeLine;

    public static void main(String[] agrs) {
        //Login
        Scanner scan = new Scanner(System.in);
        boolean active = true;
        while (active) {
            boolean loginLock;
            System.out.flush();
            do {
                System.out.print("Enter username : ");
                String username = scan.nextLine();
                System.out.print("Password : ");
                String pass = scan.nextLine();
                loginLock = !login(username, pass);
                if (loginLock) {

                    System.out.print("Do you want to create that username (y/N) ? ");
                    String answer = scan.nextLine().toLowerCase();
                    if (answer.equalsIgnoreCase("y")) {
                        try {
                            CurrentUser = new User(username, pass);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                        loginLock = !CurrentUser.save();
                        if (loginLock) {
                            System.out.println("Error detected ! Please try again !");
                        } else {
                            System.out.println(CurrentUser.getUsername() + " created successfully !");
                        }
                    }
                }
            } while (loginLock);

            //Welcome message

            CurrentUserline = new UserLine(CurrentUser.getUsername());
            CurrentTimeLine = new TimeLine(CurrentUser.getUsername());


            System.out.println("Welcome " + CurrentUser.getUsername() + "!");
            System.out.println("CassaTweet V 0.1");

            //menu
            boolean menuLock = true;
            while (menuLock) {
                System.out.print("> ");
                String command = scan.nextLine();
                switch (command) {
                    case "/exit":
                        System.out.println("exiting...");
                        System.exit(0);
                        break;
                    case "/logout":
                        menuLock = false;
                        System.out.println("Logout !");
                        break;
                    default:
                        menu(command);
                }
            }

        }
    }

    public static boolean login(String username, String pass) {
        try {
            CurrentUser = new User(username, pass);
            return CurrentUser.load();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void menu(String inputCommand) {
        Scanner input = new Scanner(System.in);
        String[] resSplit = inputCommand.split(" ", 2);
        String Command = resSplit[0].toLowerCase();
        switch (Command) {
            case "/add":
                String newNick;
                if (resSplit.length < 2) {
                    System.out.print("# Enter friend's username: ");
                    newNick = input.nextLine();
                } else {
                    newNick = resSplit[1];
                }
                CurrentUser.friend(resSplit[1]);
                System.out.println("# " + newNick + " has been added to friend_list!");
                break;
            case "/friends":
                System.out.println(CurrentUser.getFriends());
                break;
            case "/userline":
                String user;
                if (resSplit.length < 2) {
                    System.out.print("# Enter username to view userline: ");
                    user = input.nextLine();
                    if (user.isEmpty()) {
                        user = CurrentUser.getUsername();
                    }
                } else {
                    user = resSplit[1];
                }
                UserLine ul = new UserLine(user);
                ul.loadUserline(10);
                System.out.println(ul.getUserline());
                break;
            case "/timeline":
                int amount;
                if (resSplit.length < 2) {
                    amount = 10;
                } else {
                    amount = Integer.getInteger(resSplit[1]);
                }
                TimeLine tl = new TimeLine(CurrentUser.getUsername());
                tl.loadTimeline(amount);
                System.out.println(tl.getTimeLine());
                break;
            case "/h":
                System.out.println("~~ CassaTweet Help ~~\n");
                System.out.println("Available command :");
                System.out.println("/add [friends_id]       Used to add friends");
                System.out.println("/friends                List of friends");
                System.out.println("/userline [username]    View [username] tweets");
                System.out.println("/timeline [amount]      View user's current timeline (default is 10)");
                System.out.println("/logout                 Used to logout");
                System.out.println("/exit                   Quit the program");
                System.out.println("/h                      This help");
                System.out.println("~~ CassaTweet Help ~~\n");
                break;
            case "/tweet":
                String body;
                if (resSplit.length < 2) {
                    System.out.print("# Enter body untuk tweet: ");
                    body = input.nextLine();
                } else {
                    body = resSplit[1];
                }
                Tweet twit = new Tweet(CurrentUser.getUsername(), body);
                if (twit.Save()) {
                    CurrentUserline.putToUserline(twit.getUuids());
                    CurrentTimeLine.putToTimeline(twit.getUuids());

                    System.out.println("# Tweet sent!");
                } else System.out.println("! Error sending tweet!");
                break;
            default:
                System.out.println("Command error please user '/h' for help !");
        }
    }
}
